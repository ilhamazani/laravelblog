<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div>
        <h1>Buat Account Baru !</h1>
        <h3>Sign Up Form</h3>
        <form method="POST" action="{{ url('dashboard') }}">
        @csrf
            <label for="fname">Frist Name:</label><br>
            <input type="text" name="fname"><br><br>
            <label for="lname">Last Name:</label><br>
            <input type="text" name="lname"><br>
            
            <p>Gender</p>
            <input type="radio" id="male">
            <label for="male">Male</label><br>
            <input type="radio" id="female">
            <label for="female">Female</label><br>
            <input type="radio" id="other">
            <label for="other">Other</label><br><br>

            <label for="nationality">Nationality:</label><br><br>
            <select name="national">
                <option value="indonesia">Indonesia</option>
                <option value="singapura">Singapura</option>
                <option value="malaysia">Malaysia</option>
                <option value="myanmar">Myanmar</option>
            </select>

            <p>Language Spoken</p>
            <input type="checkbox" id="language">
            <label for="b-indonesia">Bahasa Indonesia</label><br>
            <input type="checkbox" id="eng">
            <label for="b-english">English</label><br>
            <input type="checkbox" id="b-other">
            <label for="b-other">Other</label><br><br>

            <label for="fbio">Bio:</label><br><br>
            <textarea name="Bio" id="bio" cols="30" rows="10"></textarea>

            <br><br>
            <input type="submit" value="Sign Up">
        </form>
    </div>
</body>
</html>