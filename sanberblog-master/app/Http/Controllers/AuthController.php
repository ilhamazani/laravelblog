<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Requests;

class AuthController extends Controller
{
    public function signUp()
    {
        return view ('signup');
    }

    public function dashboard(Request $request)
    {

        $data['fname'] = $request->fname;
        $data['lname'] = $request->lname;

        return view ('dashboard', $data);
    }
}